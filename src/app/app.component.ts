import { Component } from '@angular/core';
import firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor() {
    var firebaseConfig = {
      apiKey: "AIzaSyAH3e03-hueuQqnt7LbNT28nBRxoXyGEp0",
      authDomain: "collection-e2d4d.firebaseapp.com",
      databaseURL: "https://collection-e2d4d-default-rtdb.europe-west1.firebasedatabase.app",
      projectId: "collection-e2d4d",
      storageBucket: "collection-e2d4d.appspot.com",
      messagingSenderId: "876281579722",
      appId: "1:876281579722:web:dc12f08945de2e616d4c33"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
  }
}
