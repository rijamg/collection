import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Book } from '../models/Book.model';
import { BooksService } from '../services/books.service';


@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit, OnDestroy {

  books: Book [] = [];
  booksSubscription: any = Subscription;
  page = 1;
  count = 0;
  tableSize = 2;
  isSortByAuthor = false;
  isSortByTitle = false;
  queryString = '';




  constructor(private booksService: BooksService,
              private router: Router) { }

  ngOnInit() {
    if (this.queryString !== '' && this.queryString !== undefined) {
      this.searchByName(this.queryString);
    }
    else if (this.isSortByTitle) {
      this.sortByTitle();
    }
    else
    {
      this.sortByAuthor();
    }
    // fetch("https://www.googleapis.com/books/v1/volumes?q=le+secret+des+glaces")
    // .then(response =>response.json())
    // .then(data => {
    //   console.log(data.items[0].volumeInfo.title);
    // })
  }

  onNewBook () {
    this.router.navigate(['/books', 'new']);
  }

  onDeleteBook(book: Book) {
    this.booksService.removeBook(book);
  }

  onViewBook (id: number) {
    this.router.navigate(['/books', 'view', id]);
  }

  ngOnDestroy() {
    this.booksSubscription.unsubscribe();
  }

  choiceSortByAuthor() {
    this.isSortByAuthor = true;
    this.isSortByTitle = false;
    this.queryString = '';
    this.page = 1;
    this.ngOnInit();
  }

  choiceSortByTitle() {
    this.isSortByAuthor = false;
    this.isSortByTitle = true;
    this.queryString = '';
    this.page = 1;
    this.ngOnInit();
  }

  sortByAuthor() {
    this.booksSubscription = this.booksService.booksSubject.subscribe(
      (books: Book[]) => {
        this.books = books.sort((first, second) => 0 - (first.author > second.author ? -1 : 1));
      }
    );
    this.booksService.getBooks();
    this.booksService.emitBooks();
  }

  sortByTitle() {
    this.booksSubscription = this.booksService.booksSubject.subscribe(
      (books: Book[]) => {
        this.books = books.sort((first, second) => 0 - (first.title > second.title ? -1 : 1));
      }
    );
    this.booksService.getBooks();
    this.booksService.emitBooks();
  }

  onTableDataChange(event: any){
    this.page = event;
    this.ngOnInit();
  }

  sendName(queryString : string) {
    this.queryString = queryString;
    this.page = 1;
    this.ngOnInit();
  }

  searchByName(queryString : string) {
    this.booksService.getBooksByName(queryString);
    this.booksService.emitBooks();
  }
}
