export class Video {
  photo: string ='';
  constructor(public title: string, public description: string, public date: Date ) {}
}
