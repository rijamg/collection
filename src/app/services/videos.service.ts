import { Injectable } from '@angular/core';
import { Video } from '../models/Video.model';
import firebase from 'firebase';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VideosService {

  videos: Video[] = [];
  videosSearch: Video[] = [];
  videosSubject = new Subject<Video[]>();

  constructor() { }

  emitVideos() {
    this.videosSubject.next(this.videos);
  }

  saveVideos() {
    firebase.database().ref('/videos').set(this.videos);
  }

  getVideos() {
    firebase.database().ref('/videos')
      .on('value', (data) => {
        this.videos = data.val() ? data.val() : [];
        this.emitVideos();
      });
  }

  getVideosByName(queryString: string) {
    firebase.database().ref('/videos')
      .on('value', (data) => {
        this.videos = [];
        this.videosSearch = data.val() ? data.val() : [];
      for (let i = 0; i < this.videosSearch.length; i++) {
        if (this.videosSearch[i].title.toLowerCase().includes(queryString.toLowerCase()))
        {
          this.videos.push(this.videosSearch[i]);
        }
      }
      this.emitVideos();


      });
  }



  // getSingleVideo(id: number) {
  //   return new Promise(
  //     (resolve, reject) => {
  //       firebase.database().ref('/videos/' + id).once('value').then(
  //         (data) => {
  //           resolve(data.val());
  //         },
  //         (error) => {
  //           reject(error);
  //         }
  //       );
  //     }
  //   );
  // }

  createNewVideo(newVideo: Video) {
    this.videos.push(newVideo);
    this.saveVideos();
    this.emitVideos();
  }

  removeVideo(video: Video) {
    const videoIndexToRemove = this.videos.findIndex(
      (videoEl) => {
        if (videoEl === video) {
          return true;
        }else {
          return false;
        }
      }
    );
    this.videos.splice(videoIndexToRemove, 1);
    this.saveVideos();
    this.emitVideos();
  }

}
