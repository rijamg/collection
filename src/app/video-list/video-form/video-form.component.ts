import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from "@angular/common/http";
import { Video } from 'src/app/models/Video.model';
import { VideosService } from 'src/app/services/videos.service';

@Component({
  selector: 'app-video-form',
  templateUrl: './video-form.component.html',
  styleUrls: ['./video-form.component.scss']
})
export class VideoFormComponent implements OnInit {

  videoForm: any = FormGroup;
  apiKey = "e852faacd8d19382d25ad78f7b16f497";
  data:any = [];

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private http: HttpClient,
              private videosService: VideosService) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.videoForm = this.formBuilder.group({
      title: ['', Validators.required]
    });
  }

  onSearchVideo() {
    const title = this.videoForm.get('title').value;

    const url ='https://api.themoviedb.org/3/search/movie?api_key='+ this.apiKey + '&query=' + title + "&language=fr";

    this.http.get(url).subscribe((res)=>{
    this.data = res;
    console.log(this.data.results[0].original_title);
  })
  }

  getVideo(video: any) {
    const title = video.title;
    const description = video.overview;
    const date = video.release_date;
    const photo = "https://image.tmdb.org/t/p/w200/" + video.poster_path;
    const newVideo = new Video(title, description, date);
    newVideo.photo = photo;
    this.videosService.createNewVideo(newVideo);
    // this.router.navigate(['/videos']);
  }
}
