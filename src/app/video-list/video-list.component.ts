import { Component, OnInit } from '@angular/core';
import firebase from 'firebase';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Video } from '../models/Video.model';
import { AuthService } from '../services/auth.service';
import { VideosService } from '../services/videos.service';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss']
})
export class VideoListComponent implements OnInit {

  videos: Video [] = [];
  videosSubscription: any = Subscription;
  isAuth: any = Boolean;
  page = 1;
  count = 0;
  tableSize = 10;
  queryString = '';

  constructor(private router: Router,
              private videosService: VideosService,
              private authService: AuthService) { }

  ngOnInit(): void {
    firebase.auth().onAuthStateChanged(
      (user) => {
        if (user) {
          this.isAuth = true;
        }else {
          this.isAuth = false;
        }
      }
    );
    if (this.queryString !== '' && this.queryString !== undefined) {
      this.searchByName(this.queryString);
    }else {
      this.videosSubscription = this.videosService.videosSubject.subscribe(
        (videos: Video[]) => {
          this.videos = videos.sort((first, second) => 0 - (first.title > second.title ? -1 : 1));
        }
        );
        this.videosService.getVideos();
        this.videosService.emitVideos();
      }
  }

  onNewVideo() {
    this.router.navigate(['/videos', 'new']);
  }

  onDeleteVideo(video: Video) {
    this.videosService.removeVideo(video);
  }

  over(i: any) {
    let hidden = document.querySelector(".class"+i);
    if (hidden) {
      hidden.classList.remove("hidden")
    }
  }

  out(i: any) {
    let hidden = document.querySelector(".class"+i);
    if (hidden) {
      hidden.classList.add("hidden")
    }
  }

  onTableDataChange(event: any){
    this.page = event;
    this.ngOnInit();
  }
  sendName(queryString : string) {
    this.queryString = queryString;
    this.page = 1;
    this.ngOnInit();
  }

  searchByName(queryString : string) {
    this.videosService.getVideosByName(queryString);
    this.videosService.emitVideos();
  }
}
